/** @jsxImportSource @emotion/react */

import { ReactNode } from "react"

const Loading = ({text}: {text: string}) => {
  return (
    <div>{text}</div>
  )
}

interface Props {
  children: ReactNode
  loadingComponent: (props: {text: string}) => ReactNode  
}

const LayoutBase = ({children, loadingComponent}: Props) => {
  const LoadingComponent = loadingComponent
  return (
    <>
      {children}
      <LoadingComponent text="communicate to server" />
    </>
  )
}
