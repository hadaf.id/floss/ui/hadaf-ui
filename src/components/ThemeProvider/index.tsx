/** @jsxImportSource @emotion/react */

import { Theme, ThemeProvider as EmotionThemeProvider } from "@emotion/react"
import defaultTheme from "./default-theme"
import { ReactNode } from "react"

interface Props {
  children: ReactNode
  theme?: Theme
}

const ThemeProvider = ({children, theme}: Props) => {
  return (
    <EmotionThemeProvider theme={theme ? theme : defaultTheme}>
      {children}
    </EmotionThemeProvider>
  )
}

export default ThemeProvider
