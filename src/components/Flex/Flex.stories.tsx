import { Meta, StoryObj } from "@storybook/react";
import Flex from ".";
import ThemeProvider from "../ThemeProvider";
import React from "react";

const meta: Meta<typeof Flex> = {
  component: Flex,
  title: "Flex",
  argTypes: {
    p: {
      control: {type: "number"}
    },
    br: {
      control: {type: "number"}
    },
    bg: {
      control: {type: "color"}
    }
  },
}

export default meta

type Story = StoryObj<typeof Flex>

export const Default: Story = {
  render: (args) => (
    <ThemeProvider>
      <Flex {...args}>
        Simple Flex
      </Flex>
    </ThemeProvider>
  ),
} 
