import { CSSObject, Theme } from "@emotion/react"
import { ReactNode, } from "react"

export interface CommonProps {
  children?: ReactNode 
  css?: CSSObject | ((theme: Theme) => CSSObject) 
  className?: string
  onClick?: (e: React.MouseEvent) => void
}

export interface FlexProps {
  title?: string
  gap?: number
  br?: number
  p?: number
  px?: number
  py?: number
  pl?: number
  pr?: number
  pt?: number
  pb?: number
  m?: number
  mx?: number
  my?: number
  ml?: number
  mr?: number
  mt?: number
  mb?: number
  bg?: string
  w?: number | "auto"
  id?: string
  grow?: number
  jc?: "flex-start" | "flex-end" | "center" | "stretch" | "space-between" // Justify content
  ai?: "start" | "end" | "center" | "stretch" // Align items
}

