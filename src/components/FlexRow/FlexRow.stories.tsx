import { Meta, StoryObj } from "@storybook/react";
import ThemeProvider from "../ThemeProvider";
import React from "react";
import FlexRow from ".";
import Flex from "../Flex";
import Button from "../Button";

const meta: Meta<typeof FlexRow> = {
  component: FlexRow,
  title: "FlexRow",
  // argTypes: {
  //   p: {
  //     control: {type: "number"}
  //   },
  //   br: {
  //     control: {type: "number"}
  //   },
  //   bg: {
  //     control: {type: "color"}
  //   }
  // },
}

export default meta

type Story = StoryObj<typeof FlexRow>

export const Layout: Story = {
  render: () => (
    <ThemeProvider>
      <FlexRow gap={0} ai="stretch">
        <Flex w={300} p={1} bg="#c6c6c6">
          sidebar
          <Button fullWidth>Hehe</Button>
        </Flex>
        <Flex grow={1} p={1} bg="#e6e6e6" jc="flex-start">
          content
        </Flex>
      </FlexRow>
    </ThemeProvider>
  ),
} 
