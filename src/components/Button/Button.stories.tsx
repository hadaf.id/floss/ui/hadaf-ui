import { Meta, StoryObj } from "@storybook/react"
import Button from "." 
import React from "react"
import ThemeProvider from "../ThemeProvider"

const meta: Meta<typeof Button> = {
  component: Button,
  title: "Button",
  // argTypes: {
  //   variant: {
  //     options: ["soft", "outlined", "contained", "text", "icon", undefined],
  //     control: { type: "radio" },
  //   }
  // },
}

export default meta

type Story = StoryObj<typeof Button>

export const Default: Story = {
  render: (args) => (
    <ThemeProvider>
      <Button {...args}>
        Ini button nya wow
      </Button>
    </ThemeProvider>
  ),
} 

export const Icon: Story = {
  render: (args) => (
    <ThemeProvider>
      <Button {...args}>
        Ini button nya wow
      </Button>
    </ThemeProvider>
  ),
} 
