import '@emotion/react'

// Inspired by https://mui.com/material-ui/customization/default-theme/

declare module '@emotion/react' {
  export interface Theme {
    palette?: {
      primary?: {
        main?: string
        light?: string
        dark?: string
        contrastText?: string
      }
      secondary?: {
        main?: string
        light?: string
        dark?: string
        contrastText?: string
      }
      info?: {
        main?: string
        light?: string
        dark?: string
        contrastText?: string
      }
      success?: {
        main?: string
        light?: string
        dark?: string
        contrastText?: string
      }
      warning?: {
        main?: string
        light?: string
        dark?: string
        contrastText?: string
      }
      error?: {
        dark?: string
        800?: string
        700?: string
        main?: string
        100?: string
        light?: string
        contrastText?: string
      }
      text?: {
        primary?: string
        secondary?: string
        disabled?: string
      }
      blueGray?: {
        50?: string
        100?: string
        200?: string
        300?: string
        400?: string
        500?: string
        600?: string
        700?: string
        800?: string
        900?: string
      }
      gray?: {
        50?: string
        100?: string
        200?: string
        300?: string
        400?: string
        500?: string
        600?: string
        700?: string
        800?: string
        900?: string
      }
      yellow?: {
        50?: string
        100?: string
        200?: string
        300?: string
        400?: string
        500?: string
        600?: string
        700?: string
        800?: string
        900?: string
      }
    }
    typography?: {
      h3?: {
        fontWeight?: number
        fontSize?: string
        lineHeight?: number
        litterSpacing?: string 
      }
    }
    spacing: (v: number) => number
  }
}
